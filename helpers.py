import sympy as sym
from sympy import Matrix

import numpy as np
import mpmath as mp

from IPython.display import display

def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result 


def make_dict(keys, values):
    """
    Make dict from two lists
    """
    return dict(zip(keys, values))


def print_line(l = 50, c = '*'):
    print('\n\n')
    for i in range(l):
        print(c, end='')
    print('\n')


def canonical_f(f, s):
    num = sym.numer(f)
    den = sym.denom(f)
    
    num = num / sym.Poly(den, s).coeffs()[0]
    den = den / sym.Poly(den, s).coeffs()[0]
    
    num = num.expand()
    den = den.expand()
    
    return sym.collect(num, s)/sym.collect(den, s)


def controlable_canonical_matrices(f, s):
    f_can = canonical_f(f, s)
    num = sym.numer(f_can)
    den = sym.denom(f_can)
    coefs_num = sym.Poly(num, s).coeffs();
    coefs_den = sym.Poly(den, s).coeffs();
    
    dim = len(coefs_den) - 1
    
    A = sym.eye(dim);
    A = A.row_insert(dim, Matrix([0 for i in range(dim)]).T)
    A = A.col_insert(0, Matrix([0 for i in range(dim + 1)]))
    A.row_del(dim)
    A.col_del(dim)

    for i in range(dim):
        A[dim - 1, i] = -coefs_den[dim - i]
        
    B = Matrix([0 for i in range(dim - 1)] + [1])
    C = Matrix([_ for _ in reversed(coefs_num)]).T
    
    return (A, B, C)


def observable_canonical_matrices(f, s):
    f_can = canonical_f(f, s)
    num = sym.numer(f_can)
    den = sym.denom(f_can)
    coefs_num = sym.Poly(num, s).coeffs();
    coefs_den = sym.Poly(den, s).coeffs();
    
    dim = len(coefs_den) - 1
    
    A = sym.eye(dim);
    A = A.col_insert(dim, Matrix([0 for i in range(dim)]))
    A = A.row_insert(0, Matrix([0 for i in range(dim + 1)]).T)
    A.row_del(dim)
    A.col_del(dim)

    for i in range(dim):
        A[i, dim - 1] = -coefs_den[dim - i]

    B = Matrix([_ for _ in reversed(coefs_num)])
    C = Matrix([0 for i in range(dim - 1)] + [1]).T
    
    return (A, B, C)

def set_matrix_to(matr, matr_to, y, x):
    for i in range(matr.shape[0]):
        for j in range(matr.shape[1]):
            matr_to[y + i, x + j] = matr[i, j]
            if x + j == matr_to.shape[1] - 1:
                break;
        if y + i == matr_to.shape[0] - 1:
            break;


def apply_to_elems(M, f):
    for i in range(M.shape[0]):
        for j in range(M.shape[1]):
            M[i, j] = f(M[i, j])


def apply_to_elems_copy(M, f):
    temp = M.copy()
    for i in range(temp.shape[0]):
        for j in range(temp.shape[1]):
            temp[i, j] = f(temp[i, j])
            
    return temp


def to_numpy_matrix(M):
    return np.array(np.array(M), np.float)


def mpmath_to_matrix(M):
    matr = sym.zeros(M.rows, M.cols)
    for i in range(M.rows):
        for j in range(M.cols):
            matr[i, j] = sym.Float(M[i, j])
            
    return matr


def interv_m_to_matrix(M, getter=None, n=3):
    if (getter == None):
        return interv_m_as_matrix(M, n)
    
    matr = sym.zeros(len(M), len(M[0]))
    for i in range(len(M)):
        for j in range(len(M[i])):
            matr[i, j] = sym.N(sym.Float(getter(mp.mpi(M[i][j]))), n)

    return matr


def matrix_to_m_interv(M_left, M_right=None):
    if (M_right == None):
        M_right = M_left
        
    matr = np.empty(M_left.shape, dtype=object)
    for i in range(M_left.shape[0]):
        for j in range(M_left.shape[1]):
            a = float(M_left[i, j])
            b = float(M_right[i, j])
            matr[i, j] = mp.mpi(a, b)

    return matr


def interv_m_as_matrix(M, n=3):
    matr = sym.zeros(len(M), len(M[0]))
    for i in range(len(M)):
        for j in range(len(M[i])):
            a = sym.N(sym.Float(mp.mpi(M[i][j]).a), n)
            b = sym.N(sym.Float(mp.mpi(M[i][j]).b), n)
            matr[i, j] = sym.Interval(a, b)

    return matr


def interval_like_matrix_as_interval_m(M):
    matr = np.empty(M.shape, dtype=object)
    for i in range(M.shape[0]):
        for j in range(M.shape[1]):
            matr[i, j] = interval_to_mpi(M[i, j])

    return matr


def interval_to_mpi(inter):
    a = 0
    b = 0
    if (inter.is_FiniteSet):
        a = sym.Float(inter.atoms().pop())
        b = a
    else:
        a = inter.start
        b = inter.end
    
    return mp.mpi(a, b)


def get_f_coeffs(x):
    f0 = mp.mpi(1)
    f1 = -x[0] - x[4] - x[8]
    f2 = x[0] * x[4] + x[0] * x[8] - x[1] * x[3] - x[2] * x[6] +\
         x[4] * x[8] - x[5] * x[7]
    f3 = -x[0] * x[4] * x[8] + x[0] * x[5] * x[7] + x[1] * x[3] * x[8] -\
         x[1] * x[5] * x[6] - x[2] * x[3] * x[7] + x[2] * x[4] * x[6]

    return [f0, f1, f2, f3]


def det_generic(l):
    n=len(l)
    if (n>2):
        i=1
        t=0
        sum=0
        while t<=n-1:
            d={}
            t1=1
            while t1<=n-1:
                m=0
                d[t1]=[]
                while m<=n-1:
                    if (m==t):
                        u=0
                    else:
                        d[t1].append(l[t1][m])
                    m+=1
                t1+=1
            l1=[d[x] for x in d]
            sum=sum+i*(l[0][t])*(det_generic(l1))
            i=i*(-1)
            t+=1
        return sum
    else:
        return (l[0][0]*l[1][1]-l[0][1]*l[1][0])


def print_task_title(num):
    print('''\n\n\n
    ######################################################
    ####################### TASK {} #######################
    ######################################################
    '''.format(num))


def print_out_matr_list(d, subs, print_f=display, n=3):
    for st, M in d.items():
        print("\n" + st + " =", end="")
        M_value = M.subs(subs);
        if n != -1:
            apply_to_elems(M_value, lambda x: sym.N(x, n))
        print_f(M_value)


def print_out_matr_neasted_list(d, indexes, subs, print_f=display, n=3):
    for st, M in d.items():
        print("\n" + st + " =")
        for i in range(len(indexes)):
            print(indexes[i], end = ",", flush=True)
            if (i + 1 == len(indexes)) or (M[i + 1] != M[i]):
                if isinstance(M[i], Matrix):
                    print(":")
                    M_value = M[i].subs(subs)
                    if n != -1:
                        apply_to_elems(M_value, lambda x: sym.N(x, n))
                    print_f(M_value)
                else:
                    print(": ", end='')
                    M_value = M[i].subs(subs)
                    if n != -1:
                        print_f(sym.N(M_value, n))
                    else:
                        print_f(M_value)

        if isinstance(M[i], Matrix):
            print('\n')





