# -*- coding: utf-8 -*-

q_count = 7

a = [2, 0.6, 0, 6, 10]
b = [3, 0.4]

dt = 0.07

omega_0 = 3

disp_formulas_as_images = True

def get_num(b, q, s):
    num = b[0] * (1 + q[1]) * s + b[1] * (1 + q[2])
    return num

def get_den(a, q, s):
    den = (a[0] * (1 + q[3]) * s + a[1] * (1 + q[4])) * \
          (a[3] * (1 + q[6]) * s + a[4] * (1 + q[7]))
    return den

def get_A(a, b, q, s):
    return 0

def get_B(a, b, q, s):
    return 0

def get_C(a, b, q, s):
    return 0
    