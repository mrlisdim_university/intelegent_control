# -*- coding: utf-8 -*-
"""
Lisin Dmitriy

This is a main calc file
"""

from sympy.printing.mathml import print_mathml
import operator

import init_params

import helpers
from helpers import merge_dicts as mer_d
from helpers import make_dict as mk_d
from helpers import print_line as pl
from helpers import to_numpy_matrix as m2np
from helpers import apply_to_elems, apply_to_elems_copy,\
                    print_task_title, print_out_matr_list,\
                    print_out_matr_neasted_list, set_matrix_to,\
                    interv_m_to_matrix, interv_m_as_matrix, matrix_to_m_interv,\
                    interval_like_matrix_as_interval_m,\
                    interval_to_mpi, get_f_coeffs

import sympy as sym
from sympy import Matrix, eye, zeros, ones, N
import sympy.physics.mechanics as mec

import mpmath as mp
from mpmath import mpi as interv
mp.pretty = True

from scipy.linalg import solve_sylvester, solve_lyapunov
from scipy.optimize import minimize

import numpy as np
from numpy.linalg import svd, cond


if init_params.disp_formulas_as_images:
    from IPython.display import display
    mec.init_vprinting()
else:
    def display(s):
        print('\n')
        sym.pprint(s)
        sym.init_printing()


a = sym.symbols('a0:{}'.format(len(init_params.a)), real=True)
b = sym.symbols('b0:{}'.format(len(init_params.b)), real=True)
q = sym.symbols('q0:{}'.format(init_params.q_count + 2), real=True)
s = sym.symbols('s', real=True)

a_values = mk_d(a, init_params.a)
b_values = mk_d(b, init_params.b)
q_init = mk_d(q, [0]*len(q))

all_values = mer_d(a_values, b_values)



######################################################
####################### TASK 1 #######################
######################################################

def Phi(s):
    num = init_params.get_num(b, q, s)
    den = init_params.get_den(a, q, s)
    
    return num / den

def SimplyfiedPhi(s):
    __Phi = Phi(s)
    num = sym.numer(__Phi)
    den = sym.denom(__Phi)
    
    num = num.expand()
    den = den.expand()
    
    return sym.collect(num, s)/sym.collect(den, s)


simp_Phi = SimplyfiedPhi(s)
b_new = sym.Poly(sym.numer(simp_Phi), s).coeffs()
b_ast = mec.dynamicsymbols("b0:{}^*".format(len(b_new)))
a_new = sym.Poly(sym.denom(simp_Phi), s).coeffs()
a_ast = mec.dynamicsymbols("a0:{}^*".format(len(a_new)))

a_ast_values = mk_d(a_ast, a_new)
b_ast_values = mk_d(b_ast, b_new)
all_ast_values = mer_d(a_ast_values, b_ast_values)

def SimplePhi(s):
    __Phi = SimplyfiedPhi(s)
    num = sym.numer(__Phi)
    den = sym.denom(__Phi)
    
    num = num.subs(mk_d(b_new, b_ast))
    den = den.subs(mk_d(a_new, a_ast))
    
    return num/den

def CanonicalPhi(s):
    __Phi = SimplePhi(s)
    
    return helpers.canonical_f(__Phi, s)

canon_Phi = CanonicalPhi(s)
canon_Phi = canon_Phi.subs(all_ast_values)
canon_Phi = canon_Phi.subs(all_values)
canon_Phi = sym.nsimplify(canon_Phi.subs(q_init))


As = init_params.get_A(a, b, q, s)
Bs = init_params.get_B(a, b, q, s)
Cs = init_params.get_C(a, b, q, s)
A = B = C = None

if As == 0:
    A, B, C = helpers.controlable_canonical_matrices(canon_Phi, s)
    As, Bs, Cs = helpers.controlable_canonical_matrices(simp_Phi, s)
else:
    A = As.subs(all_values).subs(q_init);
    B = Bs.subs(all_values).subs(q_init);
    C = Cs.subs(all_values).subs(q_init);
    
Aj = []
Bj = []
Cj = []
q_indexes = []

for i in range(len(q)):
    qi = q[i]
    if sym.srepr(simp_Phi.subs(all_values)).find(sym.srepr(qi)) != -1:
        q_indexes.append(i)
        add_diff = lambda Mj, Ms, v=qi: \
            Mj.append(Ms.diff(v).subs(all_values).subs(q_init))
            
        add_diff(Aj, As)
        add_diff(Bj, Bs)
        add_diff(Cj, Cs)
    

def ranking_parameters(q_count, n, A, B, C, Aj, Bj, Cj):
    A_j_tilda = []
    B_j_tilda = []
    C_x_j_tilda = []
    C_sigma_j_tilda = []
    C_eta_j_tilda = []
    C_j_tilda = []
    
    for i in range(q_count):
        Ma = sym.diag(A, A);
        y = Ma.shape[0] // 2
        helpers.set_matrix_to(Aj[i], Ma, y, 0)
        A_j_tilda.append(Ma)
        
        Mb = sym.ones(B.shape[0] * 2, B.shape[1])
        y = Mb.shape[0] // 2
        helpers.set_matrix_to(B, Mb, 0, 0)
        helpers.set_matrix_to(Bj[i], Mb, y, 0)
        B_j_tilda.append(Mb)
    
        Mc = sym.ones(C.shape[0], C.shape[1] * 2)
        y = Mc.shape[1] // 2
        helpers.set_matrix_to(Cj[i], Mc, 0, 0)
        helpers.set_matrix_to(C, Mc, 0, y)
        C_eta_j_tilda.append(Mc)
    
        Mc = sym.zeros(A.shape[0], Ma.shape[0])
        helpers.set_matrix_to(sym.eye(Mc.shape[0]), Mc, 0, 0)
        C_x_j_tilda.append(Mc)
        
        Mc = sym.zeros(A.shape[0], Ma.shape[0])
        x = Mc.shape[1] // 2
        helpers.set_matrix_to(sym.eye(Mc.shape[0]), Mc, 0, x)
        C_sigma_j_tilda.append(Mc)
        
        Mc = sym.zeros(C.shape[0], C.shape[1] * 2)
        helpers.set_matrix_to(C, Mc, 0, 0)
        C_j_tilda.append(Mc)
        
    W_tilda = []
    for j in range(q_count):
        W = []
        for i in range(2 * n):
            if i == 0:
                W.append(C_eta_j_tilda[j] * B_j_tilda[j])
            else:
                W.append(C_eta_j_tilda[j] * A_j_tilda[j]**i * B_j_tilda[j])
        Wl = []
        for k in range(len(W)):
            Wl.append(sym.N(W[k][0,0], n=3))
        W_tilda.append(Matrix(Wl).T)
    
    svals = []
    for W in W_tilda:
        _, S, __ = mp.svd(W)
        svals.append(sym.N(sym.Float(S[0,0]), 3))

    return (A_j_tilda, B_j_tilda, C_x_j_tilda, C_sigma_j_tilda,\
            C_eta_j_tilda, C_j_tilda, W_tilda, svals)

A_j_tilda, B_j_tilda, C_x_j_tilda, C_sigma_j_tilda, C_eta_j_tilda, \
C_j_tilda, W_tilda, svals = ranking_parameters(len(q_indexes), A.shape[0], A, B,\
                                               C, Aj, Bj, Cj)

svals_sorted = sorted(mk_d(q_indexes, svals).items(), key=operator.itemgetter(1))

def print_task_1():
    print_task_title(1)
    
    print("Transfer func:")
    display(Phi(s))
    
    pl()
    print("Simplyfied transfer func:")
    display(SimplyfiedPhi(s).subs(all_values))

    pl()
    print("Simple transfer func:")
    display(SimplePhi(s))
    
    pl()
    print("Canonical ready transfer func:")
    display(CanonicalPhi(s))
    
    print("\nNumerical canonical transfer func:")
    display(canon_Phi)
    
    pl()
    print("Matrices:")
    for st, M in {"A": A, "B" : B, "C" : C}.items():
        print("\n" + st + " =", end="")
        display(M)
    
    print_out_matr_neasted_list({"Aj": Aj, "Bj" : Bj, "Cj" : Cj},
                                q_indexes, {},
                                display)
    
    pl()
    print("Tilded matrices_j:")
    print_out_matr_neasted_list({"A_j_tilda": A_j_tilda,
                                 "B_j_tilda" : B_j_tilda, 
                                 "C_x_j_tilda" : C_x_j_tilda,
                                 "C_sigma_j_tilda" : C_sigma_j_tilda,
                                 "C_sigma_j_tilda" : C_sigma_j_tilda,
                                 "C_eta_j_tilda" : C_eta_j_tilda,
                                 "C_j_tilda" : C_j_tilda},
                            q_indexes, mer_d(dt_subs, dt_value, q_init),
                            display)



######################################################
####################### TASK 2 #######################
######################################################

dt = mec.dynamicsymbols('\Delta.t')
dt_subs = {dt : dt * (1 + q[8])}
dt_value = {dt: init_params.dt}

A_up = eye(A.shape[0]) + A * dt
B_up = B * dt
C_up = C

A_up_val = A_up.subs(dt_subs).subs(dt_value).subs(q_init)
B_up_val = B_up.subs(dt_subs).subs(dt_value).subs(q_init)
C_up_val = C_up.subs(dt_subs).subs(dt_value).subs(q_init)

A_up_q = A_up.subs(dt_subs).diff(q[8]).subs(dt_value).subs(q_init)
B_up_q = B_up.subs(dt_subs).diff(q[8]).subs(dt_value).subs(q_init)
C_up_q = C_up.subs(dt_subs).diff(q[8]).subs(dt_value).subs(q_init)

A_up_tilda, B_up_tilda, C_x_up_tilda, C_sigma_up_tilda, C_eta_up_tilda, \
C_up_tilda, W_up_tilde, _ = ranking_parameters(1, A_up.shape[0], A_up_val, B_up_val, \
                                          C_up_val, [A_up_q], [B_up_q], [C_up_q])


def print_task_2():
    print_task_title(2)
    
    print("A_up, B_up, C_up matrices:")

    print_out_matr_list({"A_up": A_up, "B_up" : B_up, "C_up" : C_up},
                        mer_d(dt_subs),
                        display, -1)
    
    print_out_matr_list({"A_up": A_up, "B_up" : B_up, "C_up" : C_up},
                        mer_d(dt_subs, dt_value, q_init),
                        display)
    
    pl()
    print("A_up_q, B_up_q, C_up_q matrices:")
    print_out_matr_list({"A_up_q": A_up_q, 
                         "B_up_q" : B_up_q, 
                         "C_up_q" : C_up_q},
                        mer_d(dt_subs, dt_value, q_init),
                        display)
    
    pl()
    print("Tilded up matrices:")

    print_out_matr_list({"A_up_tilda": A_up_tilda[0], 
                         "B_up_tilda" : B_up_tilda[0], 
                         "C_eta_up_tilda" : C_eta_up_tilda[0], 
                         "C_x_up_tilda" : C_x_up_tilda[0],
                         "C_sigma_up_tilda" : C_sigma_up_tilda[0], 
                         "C_up_tilda" : C_up_tilda[0]},
                        mer_d(dt_subs, dt_value, q_init),
                        display)
    
    pl()
    print("W_up_tilde matrix:")
    display(W_up_tilde)



######################################################
####################### TASK 3 #######################
######################################################


alpha = mec.dynamicsymbols('alpha')
omega_0 = mec.dynamicsymbols('omega_0')
omega_0_value = {omega_0 : init_params.omega_0}

butter = alpha**2 + 1.4 * omega_0 * alpha + omega_0**2;
roots = sym.solve(butter.subs(omega_0_value), alpha)

Gamma =  Matrix([[ 0,             1       ],
                [-omega_0**2,  -1.4*omega_0]]).subs(omega_0_value)

H = Matrix([[1], [0]]).T

M_sylv = Matrix(solve_sylvester(-m2np(A), m2np(Gamma), m2np(B*H)))
K_fb = -H * M_sylv**-1
F = A - B * K_fb
K_g = -(C * F**-1 * B)**-1
G = B * K_g

Ac = init_params.get_A(a, b, q, s)
Bc = init_params.get_B(a, b, q, s)
Cc = init_params.get_C(a, b, q, s)

if Ac == 0:
    Ac, Bc, Cc = helpers.controlable_canonical_matrices(CanonicalPhi(s), s)

Fc = Ac - Bc * K_fb

G_q = Bc * K_g
F_q = Fc.subs(all_ast_values)
C_q = Cc.subs(all_ast_values)

F_q_j = []
G_q_j = []
C_q_j = []

for i in q_indexes:
    qi = q[i]
    if sym.srepr(simp_Phi.subs(all_values)).find(sym.srepr(qi)) != -1:
        add_diff = lambda Mj, Ms, v=qi: \
            Mj.append(Ms.diff(v).subs(all_values).subs(q_init))
            
        add_diff(F_q_j, F_q)
        add_diff(G_q_j, G_q)
        add_diff(C_q_j, C_q)


F_q_j_tilda, G_q_j_tilda, C_x_q_j_tilda, C_sigma_q_j_tilda, C_eta_q_j_tilda, \
C_q_j_tilda, Wl_q_j, _ = ranking_parameters(len(q_indexes), A.shape[0], F, G, C, \
                                               F_q_j, G_q_j, C_q_j)


def print_task_3():
    print_task_title(3);
    
    print("Butter roots:")
    for root in roots:
        print(sym.N(root, 4))

    print("\nSylv matrix:")
    display(apply_to_elems_copy(M_sylv, lambda x: sym.N(x, 3)))
    
    print("\nK feedback:")
    display(apply_to_elems_copy(K_fb, lambda x: sym.N(x, 3)))
    
    print("\nF full system:")
    display(apply_to_elems_copy(F, lambda x: sym.N(x, 3)))
    
    print("\nK_g coeff:")
    display(apply_to_elems_copy(K_g, lambda x: sym.N(x, 3)))
    
    print("\nG matrix:")
    display(apply_to_elems_copy(G, lambda x: sym.N(x, 3)))

    print("\nFq matrix:")
    display(apply_to_elems_copy(Fc, lambda x: sym.N(x, 3)))

    pl()
    print_out_matr_neasted_list({"F_q_j": F_q_j, 
                                 "G_q_j" : G_q_j, 
                                 "C_q_j" : C_q_j},
                                q_indexes, mer_d(dt_subs, dt_value, q_init),
                                display)
    
    pl()
    print("Tilded matrices_q:")
    print_out_matr_neasted_list({"F_q_j_tilda": F_q_j_tilda, 
                                 "G_q_j_tilda" : G_q_j_tilda, 
                                 "C_x_q_j_tilda" : C_x_q_j_tilda, 
                                 "C_sigma_q_j_tilda" : C_sigma_q_j_tilda,
                                 "C_eta_q_j_tilda" : C_eta_q_j_tilda, 
                                 "C_q_j_tilda" : C_q_j_tilda},
                                q_indexes, mer_d(dt_subs, dt_value, q_init),
                                display)


######################################################
####################### TASK 4 #######################
######################################################


eigen_values = list(\
                    map(\
                        lambda x: sym.N(x, 4), 
                        list(F.eigenvals().keys())
                        )
                    );
                    

M_Vandermonde = Matrix(eye(F.shape[0]));

for i in range(len(eigen_values)):
    real, imag = (eigen_values[1]**i).as_real_imag();
    for j in range(len(eigen_values)):
        if j % 2 == 0:
            M_Vandermonde[i, j] = real
        else:
            M_Vandermonde[i, j] = imag
            

delta_q_j = []
beta_q_j = []

for i in range(len(q_indexes)):
    MF = (M_Vandermonde**-1 * F_q_j[i] * M_Vandermonde);
    delta_q = 1/2 * (MF[0, 0] + MF[1, 1])
    beta_q = 1/2 * (MF[0, 1] + MF[1, 0])
    delta_q_j.append(delta_q)
    beta_q_j.append(beta_q)


S_lambda = zeros(2, len(q_indexes))
for i in range(S_lambda.shape[1]):
    v = Matrix([delta_q_j[i], beta_q_j[i]])
    set_matrix_to(v, S_lambda, 0, i)

# Singular value decomposition
U_lambda, Sigma_small_lambda, V_lambda = svd(m2np(S_lambda))
Sigma_lambda = zeros(S_lambda.shape[0], S_lambda.shape[1])
Sigma_small_lambda = Matrix(np.diag(Sigma_small_lambda))
set_matrix_to(Sigma_small_lambda, Sigma_lambda, 0, 0)
U_lambda = Matrix(U_lambda)
V_lambda = Matrix(V_lambda)

# The most adverse combination
index = 0
maximal = Sigma_lambda[0, 0]
for i in range(Sigma_lambda.shape[0]):
    for j in range(Sigma_lambda.shape[1]):
        if (maximal < Sigma_lambda[i, j]):
            maximal = Sigma_lambda[i, j]
            index = j

delta_q = V_lambda.col(j).T

def print_task_4():
    print_task_title(4)
    
    print("\nEigen values:")
    display(eigen_values)

    print("\nVandermonde matrix:")
    display(M_Vandermonde)
    
    pl()
    print("\nFunctions of sensitivity:")
    print_out_matr_neasted_list({"delta_q_j": delta_q_j, "beta_q_j" : beta_q_j},
                                q_indexes, mer_d(dt_subs, dt_value, q_init),
                                print)

    pl()
    print("\nModal sensitivity matrix:")
    display(apply_to_elems_copy(S_lambda, lambda x: sym.N(x, 3)))

    pl()
    print("\nSingular value decomposition of S_lambda:")
    print_out_matr_list({"U_lambda": U_lambda,
                          "Sigma_lambda" : Sigma_lambda, 
                          "V_lambda" : V_lambda},
                        mer_d(dt_subs, dt_value, q_init),
                        display)
    
    pl()
    print("\nThe most adverse combination:")
    print("\ndelta_q:")
    display(N(delta_q, 3))


######################################################
####################### TASK 5 #######################
######################################################

A_q = init_params.get_A(a, b, q, s)
B_q = init_params.get_B(a, b, q, s)
C_q = init_params.get_C(a, b, q, s)

if A_q == 0:
    A_q, B_q, C_q = helpers.observable_canonical_matrices(SimplePhi(s), s)

A_q_ext = zeros(A_q.shape[0] + 1, A_q.shape[1] + 1)
set_matrix_to(A_q, A_q_ext, 0, 0)
set_matrix_to(B_q, A_q_ext, 0, A_q.shape[1])
A_q_ext[A_q_ext.shape[0] - 1, A_q_ext.shape[1] - 1] = 1

B_q_ext = Matrix([0 for _ in range(A_q.shape[0])] + [1])
C_q_ext = C_q.copy().col_insert(C_q.shape[1], Matrix([0]))

dq_interv = interv(-init_params.dq, init_params.dq)
A_ext_s = A_q_ext.subs(all_ast_values).subs(all_values)

A_ext_interv = init_params.get_A_interv(dq_interv)

A_ext_interv_med = interv_m_to_matrix(A_ext_interv, lambda x: x.mid)
A_ext_interv_high = interv_m_to_matrix(A_ext_interv, lambda x: x.b)
A_ext_interv_low = interv_m_to_matrix(A_ext_interv, lambda x: x.a)
delta_A_ext = [A_ext_interv_low - A_ext_interv_med,
               A_ext_interv_high - A_ext_interv_med]

def print_task_5():
    print_task_title(5)
    
    print("Canoical from or something:")
    print_out_matr_list({"A_q": A_q,
                         "B_q" : B_q, 
                         "C_q" : C_q},
                        {},
                        display, -1)
    
    print("Extended from or something:")
    print_out_matr_list({"A_q_ext": A_q_ext,
                         "B_q_ext" : B_q_ext, 
                         "C_q_ext" : C_q_ext},
                        {},
                        display, -1)
    
    pl()
    print("Full symbolic A ext:")
    display(A_ext_s)
    
    pl()
    print("Interval A:")
    display(interv_m_as_matrix(A_ext_interv))
    
    pl()
    print("Interval form of A extended:")
    display(A_ext_interv_med)
    print("+")
    display(delta_A_ext)


######################################################
####################### TASK 6 #######################
######################################################

delta_A_ext_norm = delta_A_ext[0].norm()

omega_char = delta_A_ext_norm / init_params.dF
omega_char += 1;

while sym.floor(omega_char) % 2 != 0:
    omega_char += 1
omega_char = sym.floor(omega_char)

Lambda_0 = omega_char * Matrix([[-1,0,0], [0,-0.7,0], [0,0,-1.3]])
Labmda_0_np = m2np(Lambda_0)

A_0 = m2np(A_ext_interv_med)
H_0 = m2np(Matrix([1 for _ in range(Lambda_0.shape[1])]).T)

def min_lambda(Lambda):
    Lambda = Lambda.reshape(*Lambda_0.shape)
    def min_H_f(H):
        return cond(solve_sylvester(-A_0, Lambda, m2np(B_q_ext) * H))
    
    H_ext = minimize(min_H_f, H_0, (), method='Nelder-Mead').x
    M = solve_sylvester(-A_0, Lambda, m2np(B_q_ext) * H_ext);

    return cond(M)

H_ext_np = None
Lambda_np = None

if init_params.is_robust_system:
    H_ext_np = m2np(Matrix([[1], [0], [0]]).T)
    Lambda_np = m2np(Matrix([[0,                1,                0           ],
                             [0,                0,                1           ],
                             [-omega_char**3,  -2*omega_char**2, -2*omega_char]]))
else:
    Lambda_np = init_params.get_min_Lambda()
    try:
        Lambda_np = Lambda_np.reshape(*Lambda_0.shape)
    except:    
        Lambda_np = minimize(min_lambda, Labmda_0_np, (), method='Nelder-Mead').x;
        Lambda_np = Lambda_np.reshape(*Lambda_0.shape)
    H_ext_np = minimize(lambda x : cond(solve_sylvester(-A_0, Lambda_np, m2np(B_q_ext) * x)),
                        H_0, (), method='Nelder-Mead').x;

M_ext_np = solve_sylvester(-A_0, Lambda_np, m2np(B_q_ext) * H_ext_np)

Lambda = Matrix(Lambda_np)
H_ext = Matrix(H_ext_np)
M_ext = Matrix(M_ext_np)
if H_ext.shape[0] > H_ext.shape[1]:
    H_ext = H_ext.T

K_fb_ext = -H_ext * M_ext**-1;
F_0_ext = A_0 - B_q_ext * K_fb_ext
K_g_ext = -(C_q_ext * F_0_ext**-1 * B_q_ext)**-1
G_ext = B_q_ext * K_g_ext

delta_F_interv = matrix_to_m_interv(delta_A_ext[0], delta_A_ext[1])
F_interv = matrix_to_m_interv(F_0_ext, F_0_ext) + delta_F_interv

delta_est_F = delta_A_ext_norm / F_0_ext.norm()

F_interv_s = interv_m_to_matrix(F_interv)
flatten_F_interv = F_interv.flatten()
flatten_F_s = sym.flatten(F_interv_s)
x = sym.symbols('x0:{}'.format(len(flatten_F_s)))
subs_to_interv = mk_d(x, flatten_F_s)
subs_from_interv = mk_d(flatten_F_s, x)

F_interv_s = F_interv_s.subs(subs_from_interv)
D_interv_s = (s * eye(F_interv_s.shape[0]) - F_interv_s).det()
D_interv_s_coeffs = sym.Poly(D_interv_s, s).coeffs()
D_interv_coeffs = get_f_coeffs(flatten_F_interv)


def print_task_6():
    print_task_title(6)
    
    print("Norm of delta A:")
    display(delta_A_ext_norm)
    
    pl()
    print("Omega characteristic:")
    display(omega_char)
    
    pl()
    print("Lambda 0:")
    display(N(Lambda_0, 5))
    
    pl()
    print("Lambda:")
    display(N(Lambda, 3))
    
    print("\nH_ext:")
    display(N(H_ext, 3))
    
    print("\nM_ext:")
    display(N(M_ext, 3))
    
    print("\nK_fb_ext:")
    display(N(K_fb_ext, 3))
    
    print("\nF_0_ext:")
    display(N(F_0_ext, 3))
    
    print("\nK_g_ext:")
    display(N(K_g_ext, 3))
    
    print("\nG_ext:")
    display(N(G_ext, 3))

    pl()
    print("\nInterval estimation:")
    print("{}/{}".format(delta_A_ext_norm, F_0_ext.norm()))
    display(N(delta_est_F, 5))
    
    pl()
    print("\ndelta_F:")
    display(interv_m_as_matrix(delta_F_interv))
    
    print("\nF_ext:")
    display(interv_m_as_matrix(F_interv))

    pl()
    print("f coeffs:")
    for i in range(len(D_interv_coeffs)):
        print(str(i) + ':')
        display(D_interv_coeffs[i])



######################################################
####################### TASK 7 #######################
######################################################


F_l = F_0_ext - interv_m_to_matrix(delta_F_interv, lambda x: x.b)
F_u = F_0_ext + interv_m_to_matrix(delta_F_interv, lambda x: x.b)

A_interv = A_ext_interv[0:A.shape[0], 0:A.shape[1]]
B_interv = A_ext_interv[0:B.shape[0], A.shape[1]:A_ext_interv.shape[1]]
C_interv = A_ext_interv[A.shape[0]:A_ext_interv.shape[0], 1:A_ext_interv.shape[1]]

def get_start_mid_end_delta(M):
    sme = [interv_m_to_matrix(M, lambda x: x.a),
           interv_m_to_matrix(M, lambda x: x.mid),
           interv_m_to_matrix(M, lambda x: x.b)]

    return sme, [sme[0] - sme[1], sme[2] - sme[1]]

A_interv_st_mid_end, delta_A = get_start_mid_end_delta(A_interv)
B_interv_st_mid_end, delta_B = get_start_mid_end_delta(B_interv)
C_interv_st_mid_end, delta_C = get_start_mid_end_delta(C_interv)

omega_0_value[omega_0] = 4.75 / init_params.t_p
newton_coeffs = Matrix([[1, 2*omega_0, omega_0**2]])

A_e = Matrix([[0, -newton_coeffs[2  ]], 
              [1, -newton_coeffs[1]]]).subs(omega_0_value)
B_e = Matrix([newton_coeffs[2], 0]).subs(omega_0_value)
C_e = Matrix([[0, 1]]).subs(omega_0_value)

Q = Matrix([[100, 0], 
            [0  , 10]])

P = Matrix(solve_lyapunov(m2np(A_e), m2np(Q)))

def print_task_7():
    print_task_title(7)
    
    print('Lower bound F:', end='')
    display(N(F_l, 5))
    
    print('\nHigher bound F:', end='')
    display(N(F_u, 5))

    pl()
    print('A, B, C interval matrix in obser form:')
    print_out_matr_list({"A_interv": interv_m_as_matrix(A_interv), 
                         "B_interv" : interv_m_as_matrix(B_interv), 
                         "C_interv" : interv_m_as_matrix(C_interv)},
                        {},
                        display, -1)
    
    pl()
    print('A, B, C lower interval matrix:')
    print_out_matr_list({"A_interv_l": A_interv_st_mid_end[0], 
                         "B_interv_l" : B_interv_st_mid_end[0], 
                         "C_interv_l" : C_interv_st_mid_end[0]},
                        {},
                        display, -1)

    pl()
    print('A, B, C higher interval matrix:')
    print_out_matr_list({"A_interv_h": A_interv_st_mid_end[2], 
                         "B_interv_h" : B_interv_st_mid_end[2], 
                         "C_interv_h" : C_interv_st_mid_end[2]},
                        {},
                        display, -1)

    pl()
    print("Interval form of A:")
    display(A_interv_st_mid_end[1])
    print("+")
    display(delta_A)
    
    pl()
    print("Interval form of B:")
    display(B_interv_st_mid_end[1])
    print("+")
    display(delta_B)
    
    pl()
    print("Interval form of C:")
    display(C_interv_st_mid_end[1])
    print("+")
    display(delta_C)
    
    pl()
    print_out_matr_list({"A_e" : A_e, 
                         "B_e" : B_e, 
                         "C_e" : C_e,
                         "Q"   : Q,
                         "P"   : P},
                        {},
                        display)



########################################################
####################### PRINTING #######################
########################################################

def print_out():
#    print_task_1();
#    print_task_2();
#    print_task_3();
#    print_task_4();
#    print_task_5();
#    print_task_6();
    print_task_7();

print_out()












